#!/bin/sh

# file: Snake_River_5mb.jpg
# hash: 27e8e56f1e37ece5294c3ca2535668e72f0dc2cd

if [ $# -ne 4 ]; then
    echo "usage: $0 <tracker IP> <tracker port> <file hash> <output file>"
    echo ""
    echo "example: $0 localhost 2020 27e8e56f1e37ece5294c3ca2535668e72f0dc2cd Snake_River_5mb.jpg"
    exit 0
fi

TRACKER=$1
PORT=$2
HASH=$3
FILE=$4

mkdir -p downloads
touch downloads/$FILE

./swift -t $TRACKER:$PORT \
        -h $HASH \
        -f downloads/$FILE
