#!/bin/sh

#
# Configuration
#

if [ $# -ne 4 ]; then
	echo "usage: $0 <tracker IP> <tracker port> <listen port> <file>"
	exit 0
fi

TRACKER_IP=$1
TRACKER_PORT=$2
LISTEN_PORT=$3
FILE=$4

./swift -f $FILE -t $TRACKER_IP:$TRACKER_PORT -l $LISTEN_PORT --progress
