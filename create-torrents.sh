#!/bin/sh

mkdir -p torrents
rm -f torrents/*

for path in files/*
do
    echo "Creating torrent file for $path"
    file=`basename $path`
    ctorrent -t -u "http://localhost:7070/announce" -s torrents/$file.torrent $path
    echo ""
done
