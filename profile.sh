#!/bin/bash

# TODO: Una primera aproximacio i/o idea

# Check parameters
if [ $# -ne 1 ]; then
  echo "usage: $0 <command>"
  exit 0
fi

# Get start time
t1=$(date +"%s")

# Execute command
`$1`

# Get end time
t2=$(date +"%s")

# Output elapsed time in seconds
echo $((t2-t1))