#!/bin/sh

# Setup libswift on the sliver

# Download experiment data
curl -sS http://shellblade.net/libswift.tar.gz >> libswift.tar.gz

# Uncompress
tar -xvf libswift.tar.gz

# Change directory
cd libswift

# Give permisions to the executables
chmod +x http.sh
chmod +x leecher.sh
chmod +x seeder.sh
chmod +x profile.sh
chmod +x swift

# Install dependencies
apt-get install libevent-2.0-5

# Create files directory
mkdir files

# Download test files from: http://www.thinkbroadband.com/download.html
curl -sS http://download.thinkbroadband.com/5MB.zip >> files/5MB.zip
curl -sS http://download.thinkbroadband.com/10MB.zip >> files/10MB.zip
curl -sS http://download.thinkbroadband.com/20MB.zip >> files/20MB.zip
curl -sS http://download.thinkbroadband.com/50MB.zip >> files/50MB.zip
curl -sS http://download.thinkbroadband.com/100MB.zip >> files/100MB.zip
curl -sS http://download.thinkbroadband.com/200MB.zip >> files/200MB.zip
