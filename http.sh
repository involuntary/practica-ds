#!/bin/sh

if [ $# -ne 3 ]; then
    echo "usage: $0 <tracker IP> <tracker port> <web port>"
    exit 0
fi

HOST=$1
PORT=$2
WEBPORT=$3

./swift -t $HOST:$PORT -g 0.0.0.0:$WEBPORT -w
