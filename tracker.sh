#!/bin/sh

#
# Configuration
#

if [ $# -ne 1 ]; then
	echo "usage: $0 <listen port>"
	exit 0
fi

PORT=$1

./swift -d files -l 0.0.0.0:$PORT --progress
